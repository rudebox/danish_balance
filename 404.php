<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="padding--both">
	<div class="wrap hpad center">
    	<p>Vi kunne desværre ikke finde siden du søgte efter.</p>
    	<a href="/" class="btn btn--blue">Gå til forsiden</a>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>