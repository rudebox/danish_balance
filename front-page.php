<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/content', 'layouts'); ?>
  <?php get_template_part('parts/contact'); ?>
  <?php get_template_part('parts/instagram'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
