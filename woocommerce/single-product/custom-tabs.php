<?php 
	$specs = get_field('specifications');
	$application = get_field('application');
	$extra = get_field('extra');
 ?>

 <div class="tabs">
	
	<div class="tabs__btns flex flex--wrap flex--justify">
		<?php if ($application) : ?>
		<button data-target="tabs__item--1" id="tabs__trigger--1" class="tabs__trigger btn btn--blue is-active"><?php echo _e('Anvendelse', 'lionlab'); ?></button>
		<?php endif; ?>
		<?php if ($specs) : ?>
		<button data-target="tabs__item--2" id="tabs__trigger--2" class="tabs__trigger btn"><?php echo _e('Specifikationer', 'lionlab'); ?></button>
		<?php endif; ?>
		<?php if ($extra) : ?>
		<button data-target="tabs__item--3" id="tabs__trigger--3" class="tabs__trigger btn"><?php echo _e('Ekstra', 'lionlab'); ?></button>
		<?php endif; ?>
	</div>
	
	<?php if ($application) : ?>
	<div id="tabs__item--1" class="tabs__item is-active"> 	
		<div class="tabs__content">
			<?php 
				echo $application;
			?>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($specs) : ?>
	<div id="tabs__item--2" class="tabs__item">
		<div class="tabs__content">
		<?php 
			echo $specs;
		?>
		</div>
	</div>
	<?php endif; ?>

	<?php if ($extra) : ?>
	<div id="tabs__item--3" class="tabs__item">
		<div class="tabs__content">
		<?php 
			echo $extra;
		?>
		</div>
 	</div>
 	<?php endif; ?>

 </div>