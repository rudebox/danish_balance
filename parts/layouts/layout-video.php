<?php 
	//video src
	$video_mp4 = get_sub_field('mp4');
	$video_ogv = get_sub_field('ogv');
	$video_webm = get_sub_field('webm');

	//poster
	$poster = get_sub_field('video_poster');

	//section settings
	$margin = get_sub_field('margin');
?>

<section class="video padding--<?php echo esc_attr($margin); ?> wow fadeInUp">
	<div class="wrap hpad clearfix video__container">
	
		<video id="video" class="video__video" preload="auto" muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ" style="background-image: url(<?php echo esc_url($poster['url']); ?>)">
			<source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
			<source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>
		
		<?php if ($video_mp4) : ?>
		<a class="video__controls flex flex--hvalign" data-fancybox href="<?php echo esc_url($video_mp4); ?>">
		<?php endif; ?>

		<?php if ($video_ogv) : ?>
		<a class="video__controls flex flex--hvalign" data-fancybox href="<?php echo esc_url($video_ogv); ?>">
		<?php endif; ?>

		<?php if ($video_webm) : ?>
		<a class="video__controls flex flex--hvalign" data-fancybox href="<?php echo esc_url($video_webm); ?>">
		<?php endif; ?>
			<div class="video__controls--inner">
    			<i class="fas fa-play"></i>
			</div>
		</a>
	</div>
</section>