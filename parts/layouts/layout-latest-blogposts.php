<?php 
// Custom WP query query
$args_query = array(
  'posts_per_page' => 3,
  'order' => 'DESC'
);

$query = new WP_Query( $args_query );

//section settings
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('center');

$center = get_sub_field('center');

if ($center === true) {
  $center = 'center';
}
?>

<section class="archive padding--<?php echo esc_attr($margin); ?> wow fadeInUp">
  <div class="wrap hpad">
    <h2 class="archive__header <?php echo esc_attr($center); ?>"><?php echo $title; ?></h2>
    <div class="row flex flex--wrap">

        <?php if ($query->have_posts()): ?>
        <?php while ($query->have_posts()): $query->the_post(); ?>

          <article class="archive__item col-sm-4 flex wow fadeInUp" itemscope itemtype="http://schema.org/BlogPosting">

            <header>
              <div class="archive__wrap">
                <a itemprop="thumbnailUrl" href="<?php the_permalink(); ?>">                 
                  <?php the_post_thumbnail('blog'); ?>
                </a>
              </div>

              <h2 class="archive__title h5" itemprop="headline" title="<?php the_title_attribute(); ?>"><?php the_title(); ?>
                
              </h2>
              <p class="archive__meta blue"><time datetime="<?php the_time('c'); ?>"><?php the_time('d.m.Y'); ?></time></p> 
            </header>

            <div class="archive__excerpt">
              <?php the_excerpt(); ?>
              <span>
                <a class="btn btn--blue" href="<?php the_permalink(); ?>">Læs mere</a>
              </span>
            </div>

         </article>

        <?php endwhile; else: ?>

        <p>No posts here.</p>

      <?php endif; ?>

      <?php wp_reset_postdata(); ?>
    </div>

  </div>
</section>