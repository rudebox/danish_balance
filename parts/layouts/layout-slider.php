<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $type = get_sub_field('type');
        $logo   = get_sub_field('slides_logo');
        $image   = get_sub_field('slides_bg');
        $video_mp4   = get_sub_field('slides_video');
        $title = get_sub_field('slides_title');
        $sub_title = get_sub_field('slides_sub_heading');
        $link = get_sub_field('slides_link');
        $link_text = get_sub_field('slides_link_text');
        $caption = get_sub_field('slides_text'); ?>        

        <?php
         //image
         if ($type === 'img') : 
        ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container center white">
            <div class="slider__text">
              <?php if ($logo) : ?>
              <img class="slider__logo" src="<?php echo esc_url($logo['sizes']['medium']); ?>" alt="<?php echo $logo['alt']; ?>">
              <h2 class="slider__title slider__title--has-logo h1"><?php echo esc_html($title); ?></h2>
              <h5 class="slider__sub-title"><?php echo esc_html($sub_title); ?></h5>
              <?php else: ?>
              <h2 class="slider__title h1"><?php echo esc_html($title); ?></h2>
              <h5 class="slider__sub-title"><?php echo esc_html($sub_title); ?></h5>
              <?php endif; ?>
              <?php echo $caption; ?>
              <a class="btn btn--rounded" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
            </div>
          </div>
        </div>

        <?php endif; ?>

        <?php
          //video
          if ($type === 'video') : 
        ?>

        <div class="slider__item slider__item--video flex flex--valign">
          <div class="wrap hpad slider__container center white">
            <div class="slider__text">
              <?php if ($logo) : ?>
              <img class="slider__logo" src="<?php echo esc_url($logo['sizes']['medium']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
              <h2 class="slider__title slider__title--has-logo h1"><?php echo esc_html($title); ?></h2>
              <h5 class="slider__sub-title"><?php echo esc_html($sub_title); ?></h5>
              <?php else: ?>
              <h2 class="slider__title h1"><?php echo esc_html($title); ?></h2>
              <h5 class="slider__sub-title"><?php echo esc_html($sub_title); ?></h5>
              <?php endif; ?>
              <?php echo $caption; ?>
              <a class="btn btn--rounded" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
            </div>
          </div>
          <video id="video" class="slider__video" autoplay playsinline preload="auto" loop muted="muted" volume="0">
            <source src="<?php echo esc_url($video_mp4); ?>#t=0.1" type="video/mp4" codecs="avc1, mp4a">
            Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
          </video>
        </div>

        <?php endif; ?>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>