<?php

//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$heading = get_sub_field('header');

if (have_rows('text_img_repeater') ) : 
?>

<div class="text-image--wrapper wrap hpad padding--<?php echo esc_attr($margin); ?>">

  <?php 
    while (have_rows('text_img_repeater') ) : the_row();

    $position = get_sub_field('position');
    $img = get_sub_field('img');
    $text = get_sub_field('text');
    $title = get_sub_field('header');

    $column_class = 'text-image__content ' . $position;
    $column_class .= $position === 'left' ? ' col-sm-6 col-sm-offset-6' : ' col-sm-6'; 
  ?>

  <section class="text-image wow fadeInUp">
    <div class="wrap hpad text-image__container">
      <div class="row text-image__row">

        <div class="text-image__image <?php echo esc_attr($position); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>

        <div class="<?php echo esc_attr($column_class); ?> <?php echo esc_attr($bg); ?>--bg">
          
          <h2 class="text-image__title"><?php echo esc_html($title); ?></h2>
          <?php echo $text; ?>

        </div>

      </div>
    </div>
  </section>

  <?php endwhile; ?>

</div>

<?php endif; ?>