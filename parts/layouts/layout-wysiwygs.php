<?php 
/**
* Description: Lionlab WYSIWYGS repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$indent = get_sub_field('indent');

global $layout_count; ?>
<section id="wysiwygs-<?php echo $layout_count; ?>" class="wysiwygs <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
  <div class="wrap hpad clearfix">

    <?php if(get_sub_field('header')): ?>
      <h2 class="wysiwygs__title"><?php echo esc_html(the_sub_field('header')); ?></h2>
    <?php endif; ?>

    <?php
    if(get_sub_field('offset') !== 'Flexible') {
        $flex = false;
        if(get_sub_field('offset') === '2 to 1') {
          $offset = '2:1';
        } else {
          $offset = '1:2';
        } 
      } else {
        $flex = true;
        $offset = null;
      }

      scratch_layout_declare(get_sub_field('wysiwygs'), 2, $flex, $offset);

      while(has_sub_field('wysiwygs')) {
        if ($indent === false) :
        scratch_layout_start();
          the_sub_field('wysiwyg');
        scratch_layout_end();
        endif;
        if ($indent === true) :
          echo '<div class="row">';
            echo '<div class="col-sm-8 col-sm-offset-2 wysiwygs__col">';
              the_sub_field('wysiwyg');
            echo '</div>';
          echo '</div>';
        endif;
      }
    ?>

  </div>
</section>
