<?php 
	//section settings
	$margin = get_sub_field('margin');
	$header = get_sub_field('header');
	$center = get_sub_field('center');

	if ($center === true) {
  		$center = 'center';
	}

	if (have_rows('testimonial') ) :
?>

<section class="video padding--<?php echo esc_attr($margin); ?> wow fadeInUp">
	<div class="wrap hpad clearfix video__container">
		<?php if ($header) : ?>
		<h2 class="video__header <?php echo esc_attr($center); ?>"><?php echo esc_html($header); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
		
			<?php 

				while (have_rows('testimonial') ) : the_row();
				//type 
				$type = get_sub_field('type');

				//video src
				$video_mp4 = get_sub_field('mp4');
				$video_ogv = get_sub_field('ogv');
				$video_webm = get_sub_field('webm');

				$yt = get_sub_field('yt_link');

				//trim url for easier string replacement
				$trim_url = parse_url($yt, PHP_URL_PATH);

				//replace and strip string from videolink variable down to video ID for thumbnail use
				$video_id = str_replace('/embed/', '', $trim_url);


				$text = get_sub_field('text');
				$title = get_sub_field('title');
			 ?>
			
			<?php if ($type === 'file') : ?>
			<div class="video__item col-sm-4">
				<div class="video__wrap">

					<video id="video" class="video__video" preload="auto" muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
						<source src="<?php echo esc_url($video_mp4); ?>#t=0.1" type="video/mp4" codecs="avc1, mp4a">
						<source src="<?php echo esc_url($video_ogv); ?>#t=0.1" type="video/ogg" codecs="theora, vorbis">
						<source src="<?php echo esc_url($video_webm); ?>#t=0.1" type="video/webm" codecs="vp8, vorbis">
						Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
					</video>

					<?php if ($video_mp4) : ?>
					<a class="video__controls flex flex--hvalign" volume="0" muted="muted" data-fancybox  href="<?php echo esc_url($video_mp4); ?>">
					<?php endif; ?>

					<?php if ($video_ogv) : ?>
					<a class="video__controls flex flex--hvalign" data-fancybox href="<?php echo esc_url($video_ogv); ?>">
					<?php endif; ?>

					<?php if ($video_webm) : ?>
					<a class="video__controls flex flex--hvalign" data-fancybox href="<?php echo esc_url($video_webm); ?>">
					<?php endif; ?>
						<div class="video__controls--inner">
			    			<i class="fas fa-play"></i>
						</div>
					</a>
				</div>

				<div class="video__content">
					<h4 class="video__title"><?php echo esc_html($title); ?></h4> 
					<?php echo $text; ?>
				</div>

			</div>

			<?php elseif ($type === 'yt') : ?>
			<div class="video__item col-sm-4">
				<a class="video__wrap" data-fancybox data-type="iframe" data-src="<?php echo esc_url($yt); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;">
					<div class="video__video video__video--yt" style="background-image: url(https://img.youtube.com/vi/<?php echo esc_html($video_id); ?>/maxresdefault.jpg);"></div>	
					<div class="video__controls--inner">
	    			<i class="fas fa-play"></i>
					</div>
				</a>

				<div class="video__content">
					<h4 class="video__title"><?php echo esc_html($title); ?></h4> 
					<?php echo $text; ?>
				</div>
			</div>
			
			<?php endif; ?>

			<?php endwhile; ?>

 		</div>
	</div>
</section>
<?php endif; ?>