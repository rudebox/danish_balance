<?php 
/**
* Description: Lionlab section-img field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$img = get_sub_field('img');
$center = get_sub_field('center');

if ($center === true) {
  $center = 'center';
}

$text = get_sub_field('text');
?>

<section class="section-img padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
  <div class="wrap hpad">
    <h2 class="section-img__title <?php echo esc_attr($center); ?>"><?php echo esc_html($title); ?></h2>
    <div class="row">

      <div class="section-img__item col-sm-10 col-sm-offset-1">
          <?php echo $text; ?>
      </div>

    </div>
  </div>
</section>
