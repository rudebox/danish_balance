<?php

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('center');

if ($center === true) {
  $center = 'center';
}

$args = array(
'post_type' => 'product',
'stock' => 1,
'posts_per_page' => 8,
'orderby' =>'date',
'order' => 'DESC' 
);

$products = new WP_Query( $args );

if ( $products->have_posts() ) : ?>
<section class="products <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?> wow fadeInUp">
	<div class="wrap hpad">
		<h2 class="products__header <?php echo esc_attr($center); ?>"><?php echo esc_html($title); ?></h2>
		<div class="row products__row">

			<div class="slider__track--products is-slider">
			
				<?php while ( $products->have_posts() ) : $products->the_post(); global $product; ?>
				
				<div class="products__item products__item--slider col-sm-4">
					<a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="<?php echo the_permalink(); ?>" title="<?php the_title(); ?>">
						
						<div class="products__thumbnail">
							<?php 
								if (has_post_thumbnail( $products->post->ID )) echo get_the_post_thumbnail($products->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" width="65px" height="115px" />'; 
							?>
						</div>

						<div class="products__content">
							<a href="<?php the_permalink(); ?>">
								<h2 class="products__title h5"><?php the_title(); ?></h2>
							</a>
							<span class="products__price blue"><?php echo $product->get_price_html(); ?></span>
						</div>


						<div class="products__btn">
							<?php woocommerce_template_loop_add_to_cart( $products->post, $product ); ?>
						</div>
					</a>
				</div>

				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</div>

		</div>
	</div>
</section>
<?php endif; ?>