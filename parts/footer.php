<footer class="footer gray-dark--bg" id="footer"> 
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>

		<div class="footer__stuff flex flex--center flex--justify">
			<span class="footer__copyright">Copyright © <?php echo date('Y'); ?>. Danish Balance ApS</span> 
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/card_logos.png" alt="betalingskort">  
		</div>

	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
