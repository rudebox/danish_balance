<?php 
	$title = get_field('instagram_title', 'options');
 ?>

<section class="instagram padding--both wow fadeInUp">
	<div class="wrap hpad">
		<h2 class="instagram__title center"><?php echo $title; ?></h2> 
		<div class="row instagram__row">
		<!-- LightWidget WIDGET -->
		<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="https://cdn.lightwidget.com/widgets/3e66401f61ec51b690ff473fbbf42bda.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>


		</div>
	</div>
</section>

