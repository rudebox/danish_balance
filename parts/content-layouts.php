<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'latest-products' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-products' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'text-img' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'text-img' ); ?>

    <?php
    } elseif( get_row_layout() === 'video' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'video' ); ?>

    <?php
    } elseif( get_row_layout() === 'latest-blogposts' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-blogposts' ); ?>

    <?php
    } elseif( get_row_layout() === 'testimonials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'testimonials' ); ?>

    <?php
    } elseif( get_row_layout() === 'section-img' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'section-img' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
