<?php
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('shipping', 'options') ) :
?>

<section class="shipping padding--bottom wow fadeInUp">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			<?php while (have_rows('shipping', 'options') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');

				$image_id = $icon['ID'];
			?>
				<div class="col-sm-4 shipping__item">
					<?php echo file_get_contents($icon['url']); ?>
					<h5 class="shipping__title"><?php echo esc_html($title); ?></h5>
					<?php echo $text; ?>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>