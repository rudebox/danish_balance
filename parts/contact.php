<?php 
	$title = get_field('contact_title', 'options');
	$text = get_field('contact_text', 'options');
	$form_id = get_field('contact_form_id', 'options');
 ?>

 <section class="contact padding--top wow fadeInUp">
 	<div class="wrap hpad">
 		<div class="row">

 			<div class="col-sm-6 contact__text">
 				<h2 class="contact__title"><?php echo esc_html($title); ?></h2>	
 				<?php echo $text; ?>
 			</div>

 			<div class="col-sm-6 contact__form">
 				<?php 
					gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
				?>
 			</div>

 		</div>
 	</div>
 </section>