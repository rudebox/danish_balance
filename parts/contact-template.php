<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<div class="contact padding--both wow fadeInUp">

		<div class="wrap hpad">
			<div class="row">

				<?php 
					$title = get_field('contact_title');
					$text = get_field('contact_text');
				 ?>

				<div class="col-sm-6 contact__info">
					<div class="contact__wrap">
						<h2 class="contact__title--employees"><?php echo esc_html($title); ?></h2>
						<?php echo $text; ?>
					</div> 
					
					<div class="row flex flex--wrap">
						<?php if (have_rows('contact_employees') ) : while (have_rows('contact_employees') ) : 	the_row(); 

	 					 $img = get_sub_field('img');
	 					 $position = get_sub_field('position');
	 					 $name = get_sub_field('name');
	 					 $phone = get_sub_field('phone');
	 					 $mail = get_sub_field('mail'); 
	 					?>
						
	 					<div class="contact__employee col-sm-6">
	 						<img src="<?php echo esc_url($img['sizes']['employee']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">

	 						<div class="contact__wrap">
		 						<h5 class="contact__name"><?php echo _e($name, 'lionlab'); ?></h5>

		 						<?php if ($position) : ?>
		 						<p><?php echo esc_html($position); ?></p>
		 						<?php endif; ?>

		 						<?php if ($phone) : ?>
		 						<?php echo _e('Telefon', 'lionlab'); ?>: <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a><br>
		 						<?php endif; ?>

		 						<?php if ($mail) : ?>
		 						E-mail: <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
		 						<?php endif; ?>
	 						 </div>
	 						
	 					</div>

	 					<?php endwhile; endif; ?>
 					</div>
				</div>

				<div class="col-sm-6 contact__form contact__form--mt">
				<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
				?>
				</div>

			</div>
		</div>
	</div>
	

</main>

<?php get_template_part('parts/footer'); ?>