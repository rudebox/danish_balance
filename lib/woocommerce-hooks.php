<?php 
	//enqueue scripts and styles on woo templates only
	add_action( 'wp_enqueue_scripts', 'lionlab_disable_woocommerce_loading_css_js' );
	 
	function lionlab_disable_woocommerce_loading_css_js() {
	 
		// Check if WooCommerce plugin is active
		if( function_exists( 'is_woocommerce' ) ){
	 
			// Check if it's any of WooCommerce page
			if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) { 		
				
				//Dequeue WooCommerce styles
				wp_dequeue_style('woocommerce-layout'); 
				wp_dequeue_style('woocommerce-general'); 
				wp_dequeue_style('woocommerce-smallscreen'); 	
	 
				//Dequeue WooCommerce scripts
				wp_dequeue_script('wc-cart-fragments');
				wp_dequeue_script('woocommerce'); 
				// wp_dequeue_script('wc-add-to-cart'); 
			
				wp_deregister_script( 'js-cookie' );
				wp_dequeue_script( 'js-cookie' );

				wp_dequeue_script('mailchimp-woocommerce');
	 
			}
		}	
	}

	
	//Show cart contents / total Ajax
	add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

	function woocommerce_header_add_to_cart_fragment( $fragments ) {
		global $woocommerce;

		ob_start();

		?>
		  <a class="nav__link nav__link--cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'Se din kurv' ); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cart.svg" alt="cart">
            <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
          </a>		
        <?php

		$fragments['a.nav__link--cart'] = ob_get_clean();
		return $fragments;
	}


	//remove sorting
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

	//remoove results 
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

	//remove breadcrumbs
	remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10,0);
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20,0);
	remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10,0);

	//remove sidebar
	remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

	//remove datatabs
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

	//remove single meta
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

	//rearrangement of priorities of single product summary
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );


	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 60 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 50 );

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	

	//Change number of related products output 

	function woo_related_products_limit() {
	  global $product;
		
		$args['posts_per_page'] = 6;
		return $args;
	}

	add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
	  function jk_related_products_args( $args ) {
		$args['posts_per_page'] = 3; // 3 related products
		$args['columns'] = 1; // arranged in 1 columns
		return $args;
	}

	//custom variation link swatches hook
    add_action( 'woocommerce_single_product_summary', 'custom_action_after_single_product_before_variations_form', 54 );
	function custom_action_after_single_product_before_variations_form () {

		//ACF colour vatiations on prduct archive
		if (have_rows('colour_variations') ) :

			echo '<div class="variations-radios variations-radios--archive">';
				
				while (have_rows('colour_variations') ) : the_row(); 

					$colour = get_sub_field('colour');
					$link = get_sub_field('colour_variation_link');
					
					if ($link) :	
					echo '<a href="' . $link . '" class="variations-colour" style="background:'. $colour . ';"></a>';
					else:
					echo '<div href="' . $link . '" class="variations-colour" style="background:'. $colour . ';"></div>';
					endif;

				 endwhile;
			echo '</div>';
		 endif; 

	}
	
?>