jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });

    
  //owl slider/carousel
  var owl = $('.slider__track');

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: true,
      lazyLoad:true, 
      // nav: true,
      dots: true,
      autplaySpeed: 11000,
      autoplayTimeout: 16000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200
      // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
  });


  //owl slider/carousel
  var owl = $('.slider__track--products');

  owl.owlCarousel({
      loop: true,
      items: 3,
      autoplay: true,
      nav: true,
      dots: false,
      // mouseDrag: false,
      autplaySpeed: 9000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
      responsive : {
          // breakpoint from 0 up
          0 : {
            items: 1
          },
          // breakpoint from 480 up
          480 : {
            items: 2
          },
          // breakpoint from 768 up
          768 : {
              items: 3
          }
      }
  });


  //wrap comment form inputs
  $('.comment-form-author, .comment-form-email').wrapAll('<div class="comment-form-flex"></div>');

  $('.products__item > .woocommerce-loop-product__link > .size-woocommerce_thumbnail').wrap('<div class="products__thumbnail"></div>'); 

  //remove button class
  $('.gform_button').removeClass('button');

  //fancybox
  $('[data-fancybox]').fancybox({ 
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false,
    },
    video: {
       tpl: 
        '<video class="fancybox-video" controls muted="muted" volume="0" controlsList="nodownload" poster="{{poster}}">' +
        '<source src="{{src}}" type="{{format}}" />' +
        'Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!' +
        "</video>"
    }
  });


  //trigger hidden variation select values  
  $(document).on('change', '.variations-radios input', function() {
    $('select[name="'+$(this).attr('name')+'"]').val($(this).val()).trigger('change');
  });


  //toggle product tabs
  var $toggle = $('.tabs__trigger');


  $toggle.on('click', function(e) {
    var data = $(this).data('target'); ''
    var $content = $('.tabs__item');
    var $button = $('.tabs__trigger');


  $content.each(function() { 

    if ( $(this).attr('id') === data ) {
      $(this).fadeIn(200);
      $(this).addClass('is-active');

    } 

    else {
      $(this).removeClass('is-active');
      $(this).fadeOut(200);
    }

  });

  $button.each(function() {

    if ( $(this).attr('id') === data ) {
      $(this).addClass('is-active');
    } 

    else {
      $(this).removeClass('is-active');
    }

    });
  });  

  //active tabs button
  $('.tabs__trigger').on('click', function() {
    $('.tabs__trigger').removeClass('is-active');
    $('.tabs__trigger').removeClass('btn--blue');
    $(this).addClass('is-active');
    $(this).addClass('btn--blue');
  }); 



  var wow = new WOW({
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       false,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    scrollContainer: null,    // optional scroll container selector, otherwise use window,
    resetAnimation: true,     // reset animation on end (default is true)
    }
  );

  wow.init();

  //make divs equal height - its a work around beacasue flexbox doesn't do the job alone
  if ($(window).width() > 768) {
    $('.text-image--wrapper').each(function(){  
        var highestBox = 0;

        $(this).find('.text-image').each(function(){
            if($(this).height() > highestBox){  
                highestBox = $(this).height();  
            }
        })

        $(this).find('.text-image').height(highestBox);
    });
  }    

});
