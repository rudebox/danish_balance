<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="single padding--both">
    <div class="wrap hpad">
      <div class="row">

        <article class="single__item col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>

          <div class="center">
            <a onclick="window.history.back(-1)" class="btn btn--blue">Tilbage</a>
          </div>

        </article>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>