<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="page padding--both">
    <div class="wrap hpad">
      <div class="row">

        <article class="page__content hpad">

          <?php the_content(); ?>

        </article>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>