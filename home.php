<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="archive padding--both">
    <div class="wrap hpad">
      <div class="row flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <article class="archive__item col-sm-4 flex wow fadeInUp" itemscope itemtype="http://schema.org/BlogPosting"> 

            <header>
              <div class="archive__wrap">
                <a itemprop="thumbnailUrl" href="<?php the_permalink(); ?>">                 
                  <?php the_post_thumbnail('blog'); ?>
                </a>
              </div>

              <h2 class="archive__title h5" itemprop="headline" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
              </h2>

              <p class="archive__meta blue"><time datetime="<?php the_time('c'); ?>"><?php the_time('d.m.Y'); ?></time></p> 
            </header>

            <div class="archive__excerpt" itemprop="articleBody">
              <?php the_excerpt(); ?>
              <span>
                <a class="archive__btn btn btn--blue" href="<?php the_permalink(); ?>">
                Læs mere
                </a>
              </span>
            </div>

          </article>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>

      </div>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>